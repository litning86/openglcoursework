#include <GL/glu.h>
#include <QGLWidget>
#include <GL/glut.h>
#include <glm/glm.hpp>
#include <QMessageBox>
#include <iostream>
#include <glm/gtc/type_ptr.hpp>
#include "HelloCubeWidget.h"


typedef struct materialStruct {
  GLfloat ambient[4];
  GLfloat diffuse[4];
  GLfloat specular[4];
  GLfloat shininess;
} materialStruct;


static materialStruct brassMaterials = {
  { 0.33, 0.22, 0.03, 1.0},
  { 0.78, 0.57, 0.11, 1.0},
  { 0.99, 0.91, 0.81, 1.0},
  27.8
};

static materialStruct redPlasticMaterials = {
  {0.3, 0.0, 0.0, 1.0},
  {0.6, 0.0, 0.0, 1.0},
  {0.8, 0.6, 0.6, 1.0},
  32.0
};

static materialStruct whiteShinyMaterials = {
  { 1.0, 1.0, 1.0, 1.0},
  { 1.0, 1.0, 1.0, 1.0},
  { 1.0, 1.0, 1.0, 1.0},
  100.0
};


static materialStruct blueShinyMaterials = {
  { 0.0, 0.0, 0.5, 0.0},
  { 0.0, 0.0, 1.0, 0.0},
  { 0.0, 0.0, 1.0, 0.0},
  100.0
};


static materialStruct greenShinyMaterials = {
  { 0.0, 0.0, 0.0, 0.0},
  { 1.0, 1.0, 1.0, 1.0},
  { 0.0, 0.0, 0.0, 0.0},
  100.0
};


static materialStruct redShinyMaterials = {
  { 1.0, 0.0, 0.0, 0.0},
  { 1.0, 0.0, 0.0, 0.0},
  { 1.0, 0.0, 0.0, 0.0},
  100.0
};

// constructor
HelloCubeWidget::HelloCubeWidget(QWidget *parent)
	: QGLWidget(parent),
  _image("Marc_Dekamps.ppm")
	{ // constructor


	} // constructor

// called when OpenGL context is set up
void HelloCubeWidget::initializeGL()
	{ // initializeGL()
	// set the widget background colour
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearDepth(1.0f);
  x = 1;



	} // initializeGL()

void HelloCubeWidget::changeValue(int i)
{
  //std::cout << i;
  x = 1;
  x = x + (i*1.0)/100;
  std::cout << "Number is: " << x;
  updateGL();
}


// called every time the widget is resized
void HelloCubeWidget::resizeGL(int w, int h)
	{ // resizeGL()
	// set the viewport to the entire widget
	glViewport(0, 0, w, h);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	//glEnable(GL_COLOR_MATERIAL);
	materialStruct* p_front = &brassMaterials;

	//GLfloat light_pos[] = {2., -2.,-2., 1.};

	//GLfloat light_pos[] = {-1.93, 1.30,-1.1, 1.};

	glEnable(GL_LIGHTING); // enable lighting in general
        glEnable(GL_LIGHT0);   // each light source must also be enabled
				//glShadeModel(GL_SMOOTH);
		    glEnable(GL_NORMALIZE);
        //glEnable(GL_TEXTURE_2D);


	//glLightfv(GL_LIGHT0, GL_POSITION, light_pos);

	//GLfloat lightIntensity[] ={ 1.5f,1.5f,1.5f,1.5f};
	//GLfloat light_position[] ={ 20.0f,60.0f,30.0f,30.0f};
	//glLightfv(GL_LIGHT0 ,GL_POSITION,light_position);
	//glLightfv(GL_LIGHT0 ,GL_DIFFUSE,lightIntensity);

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, _image.Width(), _image.Height(), 0, GL_RGB, GL_UNSIGNED_BYTE, _image.imageField());

  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);



	glMatrixMode(GL_PROJECTION);
	//glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	//gluPerspective(90, (float)w/h, 1e-3, 10);
	//glOrtho(-4.0, 4.0, -4.0, 4.0, -4.0, 4.0);
	glOrtho(-1.33,1.33, -1,1,0.1,100.0);

	} // resizeGL()


void HelloCubeWidget::cube1(const materialStruct& material){


  GLfloat normals[][3] = { {1., 0. ,0.}, {-1., 0., 0.}, {0., 0., 1.}, {0., 0., -1.}, {0, 1, 0}, {0, -1, 0} };

	glMaterialfv(GL_FRONT, GL_AMBIENT,    material.ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE,    material.diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR,   material.specular);
	glMaterialf(GL_FRONT, GL_SHININESS,   material.shininess);

	glColor3f(1.0,0.0,0.0);
  glNormal3fv(normals[0]);
	glBegin(GL_POLYGON);
		glVertex3f(-1.0, -1.0, -1.0);
		glVertex3f( 1.0, -1.0, -1.0);
		glVertex3f( 1.0,  1.0, -1.0);
		glVertex3f(-1.0,  1.0, -1.0);        //red
	glEnd();

  glColor3f(0.0,0.0,1.0);
  glNormal3fv(normals[3]);
  glBegin(GL_POLYGON);
    glVertex3f( 1.0, -1.0,  1.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glVertex3f( 1.0,  1.0, -1.0);
    glVertex3f( 1.0,  1.0,  1.0);            ///blue
    glEnd();


		glColor3f(0.0,1.0,0.0);
    glNormal3fv(normals[2]);
		glBegin(GL_POLYGON);
			glVertex3f(-1.0, -1.0, 1.0);
			glVertex3f( 1.0, -1.0, 1.0);
			glVertex3f( 1.0,  1.0, 1.0);          ///green
			glVertex3f(-1.0,  1.0, 1.0);
		glEnd();



		glColor3f(3.0,0.0,1.0);
    glNormal3fv(normals[1]);
		glBegin(GL_POLYGON);
			glVertex3f( -1.0, -1.0,  1.0);
			glVertex3f( -1.0, -1.0, -1.0);
			glVertex3f( -1.0,  1.0, -1.0);
			glVertex3f( -1.0,  1.0,  1.0);            ///purple
			glEnd();


		glColor3f(0.0,1.0,1.0);
    glNormal3fv(normals[4]);
		glBegin(GL_POLYGON);
			glVertex3f( -1.0, -1.0,  1.0);
			glVertex3f( -1.0, -1.0, -1.0);
			glVertex3f(  1.0, -1.0, -1.0);
			glVertex3f(  1.0, -1.0,  1.0);            ///cyan
			glEnd();


			glColor3f(1.0,1.0,1.0);
      glNormal3fv(normals[5]);
			glBegin(GL_POLYGON);
				glVertex3f( -1.0,  1.0,  1.0);
				glVertex3f( -1.0,  1.0, -1.0);
				glVertex3f(  1.0,  1.0, -1.0);
				glVertex3f(  1.0,  1.0,  1.0);            ///white
				glEnd();

}

void HelloCubeWidget::furnaturecube(const materialStruct& material){

	glMaterialfv(GL_FRONT, GL_AMBIENT,    material.ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE,    material.diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR,   material.specular);
	glMaterialf(GL_FRONT, GL_SHININESS,   material.shininess);

	glColor3f(0.545f,0.271f,0.075f);
	glBegin(GL_POLYGON);
		glVertex3f(-1.0, -1.0, -1.0);
		glVertex3f( 1.0, -1.0, -1.0);
		glVertex3f( 1.0,  1.0, -1.0);
		glVertex3f(-1.0,  1.0, -1.0);        //red
	glEnd();

  glColor3f(0.545,0.271,0.075);
  glBegin(GL_POLYGON);
    glVertex3f( 1.0, -1.0,  1.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glVertex3f( 1.0,  1.0, -1.0);
    glVertex3f( 1.0,  1.0,  1.0);            ///blue
    glEnd();


		glColor3f(0.545,0.271,0.075);
		glBegin(GL_POLYGON);
			glVertex3f(-1.0, -1.0, 1.0);
			glVertex3f( 1.0, -1.0, 1.0);
			glVertex3f( 1.0,  1.0, 1.0);          ///green
			glVertex3f(-1.0,  1.0, 1.0);
		glEnd();



		glColor3f(0.545,0.271,0.075);
		glBegin(GL_POLYGON);
			glVertex3f( -1.0, -1.0,  1.0);
			glVertex3f( -1.0, -1.0, -1.0);
			glVertex3f( -1.0,  1.0, -1.0);
			glVertex3f( -1.0,  1.0,  1.0);            ///purple
			glEnd();


		glColor3f(0.545,0.271,0.075);
		glBegin(GL_POLYGON);
			glVertex3f( -1.0, -1.0,  1.0);
			glVertex3f( -1.0, -1.0, -1.0);
			glVertex3f(  1.0, -1.0, -1.0);
			glVertex3f(  1.0, -1.0,  1.0);            ///cyan
			glEnd();


			glColor3f(0.545,0.271,0.075);
			glBegin(GL_POLYGON);
				glVertex3f( -1.0,  1.0,  1.0);
				glVertex3f( -1.0,  1.0, -1.0);
				glVertex3f(  1.0,  1.0, -1.0);
				glVertex3f(  1.0,  1.0,  1.0);            ///white
				glEnd();

}

void HelloCubeWidget::textureCube(){


  // Here are the normals, correctly calculated for the cube faces below
  GLfloat normals[][3] = { {1., 0. ,0.}, {-1., 0., 0.}, {0., 0., 1.}, {0., 0., -1.}, {0, 1, 0}, {0, -1, 0} };


  // create one face with brassMaterials
  materialStruct* p_front = &whiteShinyMaterials;

  /*glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
  glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);


  glNormal3fv(normals[0]);
  glBegin(GL_POLYGON);
    glVertex3f( 1.0, -1.0,  1.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glVertex3f( 0.0,  1.0, -1.0);
    glVertex3f( 1.0,  1.0,  1.0);
  glEnd();


  // create one face with brassMaterials


  glNormal3fv(normals[3]);
  glBegin(GL_POLYGON);
    glVertex3f(-1.0, -1.0, -1.0);
    glVertex3f( 1.0, -1.0, -1.0);
    glVertex3f( 1.0,  1.0, -1.0);
    glVertex3f(-1.0,  1.0, -1.0);
  glEnd();

  /*  p_front = &brassMaterials;

  glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
  glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);
  */
  /*glNormal3fv(normals[2]);
  glBegin(GL_POLYGON);
    glTexCoord2f(0.0, 0.0);
    glVertex3f(-1.0, -1.0, 1.0);
    glTexCoord2f(1.0, 0.0);
    glVertex3f( 1.0, -1.0, 1.0);
    glTexCoord2f(1.0, 1.0);
    glVertex3f( 1.0,  1.0, 1.0);
    glTexCoord2f(0.0, 1.0);
    glVertex3f(-1.0,  1.0, 1.0);
  glEnd();*/

  // and the others white
  p_front = &whiteShinyMaterials;

  glMaterialfv(GL_FRONT, GL_AMBIENT,    p_front->ambient);
  glMaterialfv(GL_FRONT, GL_DIFFUSE,    p_front->diffuse);
  glMaterialfv(GL_FRONT, GL_SPECULAR,   p_front->specular);
  glMaterialf(GL_FRONT, GL_SHININESS,   p_front->shininess);

  /*glNormal3fv(normals[1]);
  glBegin(GL_POLYGON);
    glVertex3f( -1.0, -1.0,  1.0);
    glVertex3f( -1.0, -1.0, -1.0);
    glVertex3f( -1.0,  1.0, -1.0);
    glVertex3f( -1.0,  1.0,  1.0);
  glEnd();


  glNormal3fv(normals[4]);
  glBegin(GL_POLYGON);
    glVertex3f(  1.0,  1.0,  1.0);
    glVertex3f(  1.0,  1.0, -1.0);
    glVertex3f( -1.0,  1.0, -1.0);
    glVertex3f( -1.0,  1.0,  1.0);
  glEnd();*/

  glNormal3fv(normals[5]);
  glBegin(GL_POLYGON);
    glTexCoord2f(0.0, 0.0);
    glVertex3f(  1.0,  -1.0,  1.0);
    glTexCoord2f(1.0, 0.0);
    glVertex3f(  1.0,  -1.0, -1.0);
    glTexCoord2f(1.0, 1.0);
    glVertex3f( -1.0,  -1.0, -1.0);
    glTexCoord2f(0.0, 1.0);
    glVertex3f( -1.0,  -1.0,  1.0);
  glEnd();

}


void HelloCubeWidget::tableLeg(double thick,double len)  // draw one table leg
{
    glPushMatrix();
    glTranslated(0,len/2,0);
    glScaled(thick,len,thick);
    furnaturecube(redShinyMaterials);
		//cube1();
    glPopMatrix();
}

void HelloCubeWidget::table(double topWid, double topThick,double legThick,double legLen)
{
    glPushMatrix();
    glTranslated(0,legLen+0.2,0);
    glScaled(topWid, topThick,topWid);
    furnaturecube(redShinyMaterials);
		//cube1();
    //glutSolidCube(1);
    glPopMatrix();
    double dist =0.95* topWid/2.0 - legThick/2.0;
    glPushMatrix();
    glTranslated(dist,0,dist);
    tableLeg(legThick,legLen);
    glTranslated(0.0,0.0,-2*dist);
    tableLeg(legThick,legLen);
    glTranslated(-2*dist,0,2*dist);
    tableLeg(legThick,legLen);
    glTranslated(0,0,-2*dist);
    tableLeg(legThick,legLen);
    glPopMatrix();
}

void HelloCubeWidget::wall(double thickness)
{
    glPushMatrix();
    glTranslated(0.5,0.5*thickness,0.5);
    glScaled(1.0,thickness,1.0);
    cube1(brassMaterials);
    glPopMatrix();
}

void HelloCubeWidget::pyramid(float scale, const materialStruct& material){

	glMaterialfv(GL_FRONT, GL_AMBIENT,    material.ambient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE,    material.diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR,   material.specular);
	glMaterialf(GL_FRONT, GL_SHININESS,   material.shininess);

  float halfscale = 0.5*scale;
  glm::vec3 v1 = { scale, -scale, 0.0};
  glm::vec3 v2 = { scale, scale, 0.0};
  glm::vec3 v3 = { 0., 0., halfscale };

  glm::vec3 n  = glm::normalize(glm::cross(v2 - v1, v3 - v2));

  glNormal3fv(glm::value_ptr(-n));
  glBegin(GL_POLYGON);
  glVertex3f(v1[0], v1[1], v1[2]);
  glVertex3f(v2[0], v2[1], v2[2]);
  glVertex3f(v3[0], v3[1], v3[2]);
  glEnd();

  v1 = { scale, scale, 0};
  v2 = {-scale, scale, 0};


  n  = glm::normalize(glm::cross(v2 - v1, v3 - v2));


  glNormal3fv(glm::value_ptr(-n));
  glBegin(GL_POLYGON);
  glVertex3f(v1[0], v1[1], v1[2]);
  glVertex3f(v2[0], v2[1], v2[2]);
  glVertex3f(v3[0], v3[1], v3[2]);
  glEnd();

  v1 = {-scale, scale, 0};
  v2 = {-scale,-scale, 0};

  n  = glm::normalize(glm::cross(v2 - v1, v3 - v2));


  glNormal3fv(glm::value_ptr(-n));
  glBegin(GL_POLYGON);
  glVertex3f(v1[0], v1[1], v1[2]);
  glVertex3f(v2[0], v2[1], v2[2]);
  glVertex3f(v3[0], v3[1], v3[2]);
  glEnd();

  v1 = {-scale, -scale, 0};
  v2 = { scale, -scale, 0};

  n  = glm::normalize(glm::cross(v2 - v1, v3 - v2));


  glNormal3fv(glm::value_ptr(-n));
  glBegin(GL_POLYGON);
  glVertex3f(v1[0], v1[1], v1[2]);
  glVertex3f(v2[0], v2[1], v2[2]);
  glVertex3f(v3[0], v3[1], v3[2]);
  glEnd();


}

void HelloCubeWidget::spherestand()
{
  //glPushMatrix();
  //glTranslated(0.5, 0.5*0.1 ,0.5);
  glScaled(0.1, 0.1, 0.1);
  cube1(brassMaterials);
  //glPopMatrix();
}

void HelloCubeWidget::chestofdraws()
{
  glPushMatrix();
  //glTranslated(0.5, 0.5*0.1 ,0.5);
  glScaled(0.2, 0.14, 0.2);
  //cube1(brassMaterials);
  //glutSolidCube(1.5);
  //textureCube();
  //testingTexture(brassMaterials);
  glPopMatrix();
}

void HelloCubeWidget::mouseDoubleClickEvent (QMouseEvent* event )
{
	//gluLookAt(10.3,10.3,2.0,0.0,0.50,0.0,0.0,1.0,0.0);
  x = x + 0.1;
  std::cout << "TESTING";
  updateGL();
}

//void HelloCubeWidget::keyPressEvent(QKeyEvent* event)
//{

//}

void HelloCubeWidget::createScene()
{
  glPushMatrix();
  glTranslated(0.4,0,0.4);
  table(0.6,0.02,0.02,0.3);
  glPopMatrix();
  glPushMatrix();
  glTranslated(0.6,0.38,0.5);      //REMEBER USE THIS ONE YOU IDIOT
  glRotated(30,0,1,0);
  glPopMatrix();
  wall(0.02);
  glPushMatrix();
  glRotated(90.0,0.0,0.0,1.0);
  wall(0.02);
  glPopMatrix();
  glPushMatrix();
  glRotated(-90.0,1.0,0.0,0.0);
  wall(0.02);
  glPopMatrix();
  glTranslated(0.4,0.5,0.4);
  glRotated(-90.0,1.0,0.0,0.0);
  pyramid(0.2, blueShinyMaterials);
  glPopMatrix();
  glPushMatrix();
  glTranslated(0.8,-0.38,-0.3);    //From here
  //glRotated(-40,0,1,0);
  //glPopMatrix();
  //chestofdraws();
  //glutWireSphere(1.0/x, 20, 16);
  glPopMatrix();                 //To Here
  glPushMatrix();
  glTranslated(0.37,-0.3,0.4*x);
  glutWireSphere(0.1, 20, 16);
  glPopMatrix();
  glPushMatrix();
}

// called every time the widget needs painting
void HelloCubeWidget::paintGL()
	{ // paintGL()
	// clear the widget

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// You must set the matrix mode to model view directly before enabling the depth test
      	glMatrixMode(GL_MODELVIEW);
        glEnable(GL_DEPTH_TEST); // comment out depth test to observe the result
				//glEnable(GL_CULL_FACE);

        //GLfloat light_pos[] = {1.5, 7, 5., 1.};
        //GLfloat light_pos[] = {1.5, 7, -2., 1.};
        //GLfloat light_pos[] = {-1.2, 2, -2., 1.};
        GLfloat light_pos[] = {-11.7, 4., -4., 1.};
        //GLfloat light_pos[] = {-1.93, 1.30,-1.1, 1.};
        glLightfv(GL_LIGHT0, GL_POSITION, light_pos);
        glLightf (GL_LIGHT0, GL_SPOT_CUTOFF,180.);


        //GLfloat m[16];
        //for (int i = 0; i < 16; i++)m[i] = 0.0;
        //m[0] = m[5] = m[10] = 1.0;
        //m[7] = -1.0/light_pos[1];
        //glPopMatrix();


	//this->cube1();
	//this->cube2();


	glLoadIdentity();
  gluLookAt(2.3, 1.3, 2.0, 0.0, 0.25, 0.0, 0.0, 1.0, 0.0);
  //gluLookAt(1.,1.,1., 0.0,0.0,0.0, 0.0,1.0,0.0);
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
  createScene();
  glPopMatrix();
  //glPushMatrix();
  //swapBuffers();
  glTranslated(0.8,0.52,-0.3);
  glScaled(0.2, 0.14, 0.2);
  //glBindTexture(GL_TEXTURE_2D, _image.imageField());
  glEnable(GL_TEXTURE_2D);
  textureCube();
  glDisable(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, 0);

  //glRasterPos2i(-1.,-1.);
  //glDrawPixels(_image.Width(),_image.Height(),GL_RGB, GL_UNSIGNED_BYTE,_image.imageField());

	//glShadeModel(GL_SMOOTH);
	//glEnable(GL_NORMALIZE);

	// flush to screen
	glFlush();
  swapBuffers();

	} // paintGL()
