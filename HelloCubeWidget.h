#ifndef __GL_POLYGON_WIDGET_H__
#define __GL_POLYGON_WIDGET_H__ 1

#include <QGLWidget>
#include <QImage>
#include "Image.h"

struct materialStruct;

class HelloCubeWidget: public QGLWidget
	{ //

	Q_OBJECT

	public:
	HelloCubeWidget(QWidget *parent);

	void cube1(const materialStruct&);
	void furnaturecube(const materialStruct&);

	public slots:
	void changeValue(int);


	protected:
	// called when OpenGL context is set up
	void initializeGL();
	// called every time the widget is resized
	void resizeGL(int w, int h);
	void mouseDoubleClickEvent ( QMouseEvent * event );
	void sliderMoveEvent(QMouseEvent* sliderEvent);
	// called every time the widget needs painting
	void paintGL();

	private:


	void tableLeg(double, double);
	void table(double, double, double, double);
	void wall(double);
	void pyramid(float, const materialStruct&);
	void spherestand();
	void textureCube();
	void testingTexture(const materialStruct&);
	void chestofdraws();
	void polygon(int, int, int, int);
	void createScene();


	Image   _image;

			QImage* p_qimage;


	double x;

	}; // class GLPolygonWidget

#endif
