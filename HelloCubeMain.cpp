#include <QApplication>
#include <QVBoxLayout>
#include <GL/glut.h>
#include "HelloCubeWindow.h"

int main(int argc, char *argv[])
	{ // main()
	// create the application
	glutInit(&argc,argv);
	QApplication app(argc, argv);

	// create model (polygon) as a triangle
	//	GLPolygon *polygon = new GLPolygon();

	// create a master widget
       	HelloCubeWindow *window = new HelloCubeWindow(NULL);

	// create a controller to hook things up
	//	GLPolygonController *controller = new GLPolygonController(window, polygon);

	// resize the window
	//window->resize(512, 612);
	window->resize(640, 480);

	// show the label
	window->show();

	// start it running
	app.exec();

	// clean up
	//	delete controller;
	delete window;

	// return to caller
	return 0;
	} // main()
